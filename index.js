const AWS = require('aws-sdk');
const Pipeline = require('./src/pipeline');

const workflow = JSON.parse(process.env.ARGO_TEMPLATE);
const parameters = workflow.inputs.parameters;
let operations = [];
let inputArtifact = {};
let outputArtifact = {};

parameters.map(param => {
    switch (param.name) {
        case 'operations':
            operations = param.value.split(',');
            return;
        case 'inputArtifact':
            inputArtifact = JSON.parse(param.value);
            return;
        case 'outputArtifact':
            outputArtifact = JSON.parse(param.value);
            return;
        default:
    }
});

const pipeline = new Pipeline(inputArtifact, outputArtifact, operations);
pipeline.process()
    .then((err, data) => {
        console.log(data);
    })
    .catch((err) => {
        console.log(err);
    });