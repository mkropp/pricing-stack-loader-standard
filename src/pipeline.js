const { pipeline } = require('stream');
const S3Connector = require('./s3Connector');
const wire = require('./wire');

class Pipeline {
    constructor(inputArtifact, outputArtifact, operations) {
        this.inputArtifact = inputArtifact;
        this.outputArtifact = outputArtifact;
        this.operations = operations;
    }

    getStreams() {
        return S3Connector.getReader(this.inputArtifact).then(input => {
            return S3Connector.getWriter(this.outputArtifact).then(output => {
                return Promise.resolve({input, output});
            })
        })
    }

    process() {
        return this.getStreams().then(({input, output}) => {
            return new Promise((resolve, reject) => {
                pipeline(
                    input,
                    wire,
                    output,
                    (err) => {
                        if (err) {
                            reject('pipeline failed: ' + err, null);
                        } else {
                            resolve(null, 'pipeline finished');
                        }
                    }
                );
            });
        });
    }
}

module.exports = Pipeline;