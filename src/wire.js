const { Transform } = require('stream');

const wire = new Transform({
    transform(chunk, encoding, callback) {
        callback(null, chunk);
    }
})

module.exports = wire;