const AWS = require('aws-sdk');
const stream = require('stream');

AWS.config.update({
    accessKeyId: 'AKIA4VJDFQ6YA3AWJG7H',
    secretAccessKey: '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',
});

function getReader (sourceArtifact) {
    return new Promise((resolve, reject) => {
        const bucket = {
            Bucket: sourceArtifact.bucket,
            Key: sourceArtifact.file,
        };

        console.log('bucket: ' + sourceArtifact.bucket);
        console.log('key: ' + sourceArtifact.file);

        const s3 = new AWS.S3();

        s3.headObject(bucket, (error, data) => {
            if (error)
                reject(error);

            const stream = s3.getObject(bucket).createReadStream();
            resolve(stream);
        });
    });
}

function getWriter (targetArtifact) {
    return new Promise((resolve, reject) => {
        const params = {
            Bucket: targetArtifact.bucket,
            Key: targetArtifact.file,
        };
        console.log('bucket: ' + targetArtifact.bucket);
        console.log('key: ' + targetArtifact.file);

        const s3 = new AWS.S3({ params: {
                Bucket: targetArtifact.bucket,
                Key: targetArtifact.file,
            }
        });

        const pass = new stream.PassThrough();

        s3.upload({Body: pass})
            .on('httpUploadProgress', function (evt) { console.log(evt); })
            .send(function (err, data) { console.log(err, data) });

        resolve(pass);
    });
}

module.exports = { getReader, getWriter };