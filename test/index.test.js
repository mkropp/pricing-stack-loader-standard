const assert = require('chai').assert;
const Pipeline = require('../src/pipeline');

describe('Pipeline', function () {
    it('should connect to S3 and transfer to second bucket', function(done) {
        const inputArtifact = {
            endpoint: 's3.amazonaws.com',
            bucket: 's3-pricing-sample',
            file: 'file1.parquet',
        }

        const outputArtifact = {
            endpoint: 's3.amazonaws.com',
            bucket: 's3-pricing-sample',
            file: 'file2.parquet',
        }
        const pipeline = new Pipeline(inputArtifact, outputArtifact, []);
        pipeline.process()
            .then((err, data) => {
                console.log(data);
                done();
            })
            .catch((err) => {
                console.log(err);
                done(err);
            });
    })
})